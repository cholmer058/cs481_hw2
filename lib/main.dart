import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
} // Main

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
        padding: const EdgeInsets.only(top: 40, left: 40, right: 8),
        child: Row(
            children: [
            _buildCharacterData('Po Bambles', 'Swordsman', 5, 'Bandit', 'Lord'),
            new Image.asset(
            'assets/character.png',
            width: 200,
            height: 300,
            ),
          ],
        ),
    );
    Color color = Colors.black45;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildBottomColumn(color, Icons.favorite, '100'),
          _buildBottomColumn(color, Icons.fitness_center, '14'),
          _buildBottomColumn(color, Icons.security, '11'),
          _buildBottomColumn(color, Icons.directions_run, '20'),
          _buildBottomColumn(color, Icons.whatshot, '5'),
        ],
      ),
    );
    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework~'),
        ),
        body: ListView(
            children: [
              Container(padding: const EdgeInsets.only(top: 40),
                child:
                titleSection,
              ),
              Container(padding: const EdgeInsets.only(top: 64),
                child:
                buttonSection,
              ),
              //textSection
            ],
        ),
      ),
    );
  } // Widget
}

Expanded _buildCharacterData(String name, String className, int level, String faction, String rank) {
  return Expanded(
    child:
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.only(top: 16),
            child: Text('Character', style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold),
            ),
          ),
          Text(name, style: TextStyle(
            fontSize: 16,
            color: Colors.blueGrey[500],),
          ),
          Container(
            padding: const EdgeInsets.only(top: 16),
            child: Text('Class', style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold),
            ),
          ),
          Text(className, style: TextStyle(
            fontSize: 16,
            color: Colors.blueGrey[500],),
          ),
          Container(
            padding: const EdgeInsets.only(top: 16),
            child: Text('Level', style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold),
            ),
          ),
          Text(level.toString(), style: TextStyle(
            fontSize: 16,
            color: Colors.blueGrey[500],),
          ),
          Container(
            padding: const EdgeInsets.only(top: 16),
            child: Text('Faction', style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold),
            ),
          ),
          Text(faction, style: TextStyle(
            fontSize: 16,
            color: Colors.blueGrey[500],),
          ),
          Container(
            padding: const EdgeInsets.only(top: 16),
            child: Text('Rank', style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold),
            ),
          ),
          Text(rank, style: TextStyle(
          fontSize: 16,
          color: Colors.blueGrey[500],),
        ),
      ],
    ),
  );
}

Column _buildBottomColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Icon(icon, color: color),
      Container(
        margin: const EdgeInsets.only(top: 8),
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      ),
    ],
  );
}